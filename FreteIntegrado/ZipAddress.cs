﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FreteIntegrado
{
    public class ZipAddress
    {
        public string StreetAddress { get; set; }
        public string Neighborhood { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
    }
}
