﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using Newtonsoft.Json;

namespace FreteIntegrado
{
    public class Servico
    {
        string Token { get; set; }
        const string BaseUrl = "https://freteintegrado.com.br/api/";

        public Servico(string token)
        {
            Token = token;
        }

        public ZipAddress GetZipCode(string zipCode)
        {
            string lcUrlGet = BaseUrl + Token + "/zip/" + zipCode;
            string lcPostData = "";
            string resposta = Requisicao(lcUrlGet, lcPostData, "GET");

            ZipAddress ret = JsonConvert.DeserializeObject<ZipAddress>(resposta);
            return ret;
        }

        public bool CreatePreshipping(Preshipping parametros)
        {
            string lcUrlGet = BaseUrl + Token + "/preshipment";
            string lcPostData = JsonConvert.SerializeObject(parametros, Formatting.None);
            string resposta = Requisicao(lcUrlGet, lcPostData, "POST");

            bool ret = JsonConvert.DeserializeObject<bool>(resposta);
            return ret;
        }

        public bool DeleteAllPreshipping()
        {
            string lcUrlGet = BaseUrl + Token + "/preshipment/all";
            string lcPostData = "";
            string resposta = Requisicao(lcUrlGet, lcPostData, "DELETE");

            bool ret = JsonConvert.DeserializeObject<bool>(resposta);
            return ret;
        }

        string Requisicao(string url, string postData, string method)
        {
            // *** Criando as propriedades da requisição
            HttpWebRequest loHttp =
                 (HttpWebRequest)WebRequest.Create(url);

            loHttp.ContentType = "application/json";
            loHttp.Method = method;
            byte[] lbPostBuffer = System.Text.Encoding.GetEncoding("UTF-8").GetBytes(postData);
            loHttp.ContentLength = lbPostBuffer.Length;

            if (method != "GET")
            {
                Stream loPostData = loHttp.GetRequestStream();
                loPostData.Write(lbPostBuffer, 0, lbPostBuffer.Length);
                loPostData.Close();
            }

            //Mandando o request
            HttpWebResponse loWebResponse = (HttpWebResponse)loHttp.GetResponse();

            Encoding enc = System.Text.Encoding.GetEncoding("UTF-8");

            StreamReader loResponseStream = new StreamReader(loWebResponse.GetResponseStream(), enc);

            //Lendo a resposta
            string resposta = loResponseStream.ReadToEnd();

            loWebResponse.Close();
            loResponseStream.Close();

            return resposta;
        }
    }
}
