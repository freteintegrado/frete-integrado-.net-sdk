﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FreteIntegrado
{
    public class Preshipping
    {
        public string ClientId { get; set; }
        public string SenderName { get; set; }
        public string SenderStreetAddress { get; set; }
        public int? SenderStreetNumber { get; set; }
        public string SenderStreetNumberAdjunct { get; set; }
        public string SenderNeighborhood { get; set; }
        public string SenderCity { get; set; }
        public string SenderState { get; set; }
        public string SenderZip { get; set; }
        public string RecipientName { get; set; }
        public string RecipientStreetAddress { get; set; }
        public int? RecipientStreetNumber { get; set; }
        public string RecipientStreetNumberAdjunct { get; set; }
        public string RecipientNeighborhood { get; set; }
        public string RecipientCity { get; set; }
        public string RecipientState { get; set; }
        public string RecipientZip { get; set; }
        public int? Weight { get; set; } //Weight in grams
        public int? Height { get; set; } //Height in centimeters
        public int? Width { get; set; } //Width in centimeters
        public int? Length { get; set; } //length in centimeters
        public string Service { get; set; }
        public string Document { get; set; }
        public string ReferenceCode { get; set; }
        public int? Insurance { get; set; }
    }
}
